import app from './app';

/*eslint no-console: 0*/
let server = app().listen(
  7777,
  '0.0.0.0',
  () => console.log('Running: http://localhost:7777 [ OK ]')
);
