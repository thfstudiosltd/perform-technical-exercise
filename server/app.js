import compress from 'compression';
import express from 'express';
import helmet from 'helmet';
import { join as joinPath } from 'path';
import routes from './routes';

export default () => express()
	.use(express.static(joinPath(__dirname, '../public')))
	.use(compress())
	.use(helmet())
	.set('views', joinPath(__dirname, '../views'))
	.set('view engine', 'jade')
	.use('/', routes);
