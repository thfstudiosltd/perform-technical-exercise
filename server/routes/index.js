import express from 'express';
import controllerHome from '../controllers/home';

let routes = express.Router();

routes.get('/', controllerHome);

export default routes;
