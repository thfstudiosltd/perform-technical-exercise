import React from 'react';

import Menu from '../menu';
import Navigation from '../Navigation';
import PageTitle from '../page-title';
import Events from '../events';

import storeNavigation from '../store/navigation';
import storeEvents from '../store/events';

export default class App extends React.Component {

	constructor() {
        super();
		this.state = { day: 'today', events: storeEvents.today };
	}

	dayChanged(day) {

		this.setState({ day: day });

		switch (day) {
			case 'today':
				this.setState({ events: storeEvents.today });
				break;
			case 'tomorrow':
				this.setState({ events: storeEvents.tomorrow });
				break;
			case 'wednesday':
				this.setState({ events: storeEvents.wednesday });
				break;
			case 'thursday':
				this.setState({ events: storeEvents.thursday });
				break;
			case 'friday':
				this.setState({ events: storeEvents.friday });
				break;
			case 'saturday':
				this.setState({ events: storeEvents.saturday });
				break;
			case 'sunday':
				this.setState({ events: storeEvents.sunday });
				break;
			default:
				this.setState({ events: [] });
		}
	}

	render() {
        return (
            <section>
				<Menu text='Menu' />
				<PageTitle h1='Schedule' h2='December' />
				<Navigation items={storeNavigation.items} onDayChanged={(day) => this.dayChanged(day) } />
				<Events events={this.state.events} />
			</section>
		)
	}
}
