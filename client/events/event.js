import React from 'react';

export default class Event extends React.Component {

	constructor (props) {
        super(props);
    }

	render() {
		const imageSrc = `/images/${this.props.event.image}`;
		const info = `${this.props.event.type} | ${this.props.event.period}`;
		const isLive = this.props.event.when.toLowerCase() === 'live' ? true : false;
		const whenClass = isLive ? 'event-when__live' : 'event-when__day';
		const metaClass = isLive ? 'event-meta event-meta--alt' : 'event-meta';

		return(
			<li className="event">
				<div className="event-image">
					<img className="event-image__thumb" src={imageSrc} alt="" />
					<p className="event-when">
						<span className={whenClass}>{this.props.event.when}</span>
						<span className="event-when__time">{this.props.event.time}</span>
					</p>
				</div>
				<div className={metaClass}>
					<p className="event-meta__info">{info}</p>
					<p className="event-meta__name">
						<span>{this.props.event.name_1}</span>
						<span>{this.props.event.name_2}</span>
					</p>
				</div>
			</li>
		);
	}
};
