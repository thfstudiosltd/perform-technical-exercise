import React from 'react';
import Event from './event.js';

export default class Events extends React.Component {

	constructor (props) {
        super(props);
    }

	render() {

		return(
			<section className="events-list">
				<ol className="events">
					{this.props.events.map((item) => {
						return(
							<Event key={item.id} event={item} />
						);
					})}
				</ol>
			</section>
		);
	}
};
