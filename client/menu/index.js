import React from 'react';

export default class Menu extends React.Component {

	constructor () {
        super();
    }

	render() {
		return(
			<section className="menu">
				<button className="menu__button">{ this.props.text }</button>
			</section>
		)
	}
}
