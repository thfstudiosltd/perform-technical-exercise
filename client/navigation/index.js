import React from 'react';

export default class Navigation extends React.Component {

	constructor (props) {
        super(props);
		this.state = { selectedId: 0, items: props.items };
    }

	itemClicked(index, name) {
		var length = this.state.items.length;
		while (length--) {
			this.state.items[length].selected = false;
		}
		this.state.items[index].selected = true;
		this.setState({ items: this.state.items });
		this.props.onDayChanged(name.toLowerCase());
	}

	render() {

		return(
			<ul className="navigation">
				{this.state.items.map((item) => {
					var cssButton = 'navigation__button';
					if (this.state.items[item.id].selected) {
						cssButton += ' navigation__button--selected';
					}
					var cssName = 'navigation__name';
					if (this.state.items[item.id].selected) {
						cssName += ' navigation__name--selected';
					}
					return (
						<li className="navigation__item" key={item.id}>
							<button className={cssButton} onClick={ () => this.itemClicked(item.id, item.name) }>
								<span className="navigation__date">{item.date}</span>
								<p className={cssName}>{item.name}</p>
								<span className="navigation__events">{item.events} events</span>
							</button>
						</li>
					);
				})}
			</ul>
		)
	}
}
