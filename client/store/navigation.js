export default {

	items: [
		{
			id: 0,
			name: 'Today',
			date: 12,
			events: 34,
			selected: true
		},
		{
			id: 1,
			name: 'Tomorrow',
			date: 13,
			events: 22,
			selected: false
		},
		{
			id: 2,
			name: 'Wednesday',
			date: 14,
			events: 45,
			selected: false
		},
		{
			id: 3,
			name: 'Thursday',
			date: 15,
			events: 14,
			selected: false
		},
		{
			id: 4,
			name: 'Friday',
			date: 16,
			events: 27,
			selected: false
		},
		{
			id: 5,
			name: 'Saturday',
			date: 17,
			events: 24,
			selected: false
		},
		{
			id: 6,
			name: 'Sunday',
			date: 18,
			events: 19,
			selected: false
		}
	]
};
