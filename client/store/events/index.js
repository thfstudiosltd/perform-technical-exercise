import eventsToday from './today.js';
import eventsTomorrow from './tomorrow.js';
import eventsWednesday from './wednesday';
import eventsThursday from './thursday';
import eventsFriday from './friday';
import eventsSaturday from './saturday';
import eventsSunday from './sunday';

export default {
	today: eventsToday.events,
	tomorrow: eventsTomorrow.events,
	wednesday: eventsWednesday.events,
	thursday: eventsThursday.events,
	friday: eventsFriday.events,
	saturday: eventsSaturday.events,
	sunday: eventsSunday.events
};
