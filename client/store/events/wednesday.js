export default {

	events: [
		{
			id: 0,
			name_1: 'St Louis Rams',
			name_2: 'at Tennessee Titans',
			when: 'Live',
			time: '12:00',
			type: 'NHL',
			period: 'Week 7',
			image: 'nhl2.jpg'
		},
		{
			id: 1,
			name_1: 'Canadian Grand Prix',
			name_2: '',
			when: 'Live',
			time: '12:00',
			type: 'F1 Championship',
			period: 'Round 7',
			image: 'f1.jpg'
		},
		{
			id: 2,
			name_1: 'LA Kings',
			name_2: 'at Montreal Canadiens',
			when: 'Live',
			time: '13:00',
			type: 'NHL',
			period: 'Week 7',
			image: 'nhl2.jpg'
		},
		{
			id: 3,
			name_1: 'Liverpool',
			name_2: 'vs Norwich',
			when: 'Live',
			time: '14:00',
			type: 'FA Cup',
			period: 'Quarter Final',
			image: 'football2.jpg'
		},
		{
			id: 4,
			name_1: 'Watford',
			name_2: 'vs Bournemouth AFC',
			when: 'Live',
			time: '17:00',
			type: 'FA Cup',
			period: 'Quarter Final',
			image: 'football.jpg'
		},
		{
			id: 5,
			name_1: 'New England Patriots',
			name_2: 'at NY Giants',
			when: 'Live',
			time: '17:00',
			type: 'NFL',
			period: 'Week 7',
			image: 'nfl2.jpg'
		},
		{
			id: 6,
			name_1: 'Manchester Utd',
			name_2: 'vs Arsenal',
			when: 'Today',
			time: '18:00',
			type: 'FA Cup',
			period: 'Quarter Final',
			image: 'football.jpg'
		},

		{
			id: 7,
			name_1: 'Sunderland',
			name_2: 'vs Swansea City AFC',
			when: 'Today',
			time: '12:00',
			type: 'FA Cup',
			period: 'Quarter Final',
			image: 'football.jpg'
		},
		{
			id: 8,
			name_1: 'Cincinatti Bengals',
			name_2: 'at Arizona Cardinals',
			when: 'Today',
			time: '16:00',
			type: 'NHL',
			period: 'Week 7',
			image: 'nhl.jpg'
		},
		{
			id: 9,
			name_1: 'St Louis Rams',
			name_2: 'at Tennessee Titans',
			when: 'Today',
			time: '16:00',
			type: 'NHL',
			period: 'Week 7',
			image: 'nhl.jpg'
		}
	]
};
