import React from 'react';

export default class PageTitle extends React.Component {

	constructor () {
        super();
    }

	render() {
		return(
			<header className="page-title">
				<h1 className="page-title__main">{ this.props.h1 }</h1>
				<h2 className="page-title__secondary">{ this.props.h2 }</h2>
			</header>
		)
	}
}
