# Perform Technical Exercise

# About
This is a Node & React (client-side) application that runs on localhost on port 7777 and is a single page schedule viewer for Perform. Mobile-first responsiveness approach taken, though allowing more time this would be exchanged with a content-first approach for further optimisations. Mobile-sized screens and up to large desktop screens are catered for through breakpoints where content appearance begins to degrade. State is passed around the app from parent to child React components.

## Pre-requisites
Node
NPM

## Installation
`npm i`

## Running
`npm start`

## With more time...
* I would implemented proper state management, i.e. Redux to appropriately handle the changes in state throughout the application and data stores/actions.
* Secondly I would take a content-first approach so as to not load heavy assets such as images on initial page-load and instead deliver core content first followed by more rich-media assets when possible to provide the user a more efficient UX.
* Add tests. I would use Jasmine or Karma to add some unit & functional testing to the JS.
